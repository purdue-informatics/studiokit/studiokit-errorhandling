﻿namespace StudioKit.ErrorHandling;

public static class ExceptionConstants
{
	/// <summary>
	/// "Violation of %ls constraint '%.*ls'. Cannot insert duplicate key in object '%.*ls'."
	/// https://msdn.microsoft.com/en-us/library/ms151757.aspx
	/// </summary>
	public const int SqlExceptionDuplicateKeyViolation = 2627;

	/// <summary>
	/// "Cannot insert duplicate key row in object '%.*ls' with unique index '%.*ls'."
	/// https://msdn.microsoft.com/en-us/library/ms151779.aspx
	/// </summary>
	public const int SqlExceptionUniqueIndexViolation = 2601;
}