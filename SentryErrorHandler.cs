﻿using Sentry;
using StudioKit.ErrorHandling.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace StudioKit.ErrorHandling;

/// <summary>
/// IErrorHandler implementation which sends exceptions to Sentry.
/// </summary>
public class SentryErrorHandler : IErrorHandler
{
	private static readonly List<Regex> ExcludeRegexPatterns = new()
	{
		new Regex(@"A public action method.*was not found on controller.*"),
		new Regex(@"The controller for path.*was not found or does not implement IController."),
		new Regex(@"A potentially dangerous Request.Path value was detected from the client.*")
	};

	public void CaptureException(Exception ex)
	{
		CaptureException(ex, null, null);
	}

	public void CaptureException(Exception ex, IExceptionUser user)
	{
		CaptureException(ex, user, null);
	}

	public void CaptureException(Exception exception, IExceptionUser exceptionUser, object extra)
	{
		if (ExcludeRegexPatterns.Any(r => r.IsMatch(exception.Message)))
			return;

		SentrySdk.ConfigureScope(scope =>
		{
			var user = scope.User.Clone();
			user.Email = exceptionUser?.Email;
			scope.User = user;
		});

		SentrySdk.CaptureException(exception);
	}
}