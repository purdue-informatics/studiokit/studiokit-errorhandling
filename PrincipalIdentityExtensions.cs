﻿using StudioKit.ErrorHandling.Interfaces;
using System.Security.Claims;
using System.Security.Principal;

namespace StudioKit.ErrorHandling;

public static class PrincipalIdentityExtensions
{
	public static IExceptionUser ToExceptionUser(this IIdentity identity)
	{
		var claimsIdentity = identity as ClaimsIdentity;
		return new ExceptionUser
		{
			Id = claimsIdentity?.FindFirst(ClaimTypes.NameIdentifier)?.Value,
			UserName = claimsIdentity?.FindFirst(ClaimTypes.Name)?.Value,
			Email = claimsIdentity?.FindFirst(ClaimTypes.Email)?.Value
		};
	}
}