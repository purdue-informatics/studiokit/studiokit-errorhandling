﻿using StudioKit.ErrorHandling.Interfaces;

namespace StudioKit.ErrorHandling;

public class ExceptionUser : IExceptionUser
{
	public string Id { get; set; }

	public string UserName { get; set; }

	public string Email { get; set; }
}