﻿using StudioKit.ErrorHandling.Interfaces;

namespace StudioKit.ErrorHandling;

public static class ErrorHandler
{
	/// <summary>
	/// Convenience property for accessing a static shared instance of IErrorHandler, instead of using DI.
	/// </summary>
	public static IErrorHandler Instance { get; set; }
}